package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop has been produced, lead to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and gives false, at the end and gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private int start;
    private int stop;
    private int count = 1;

    public RangeGenerator(int start, int stop){
        this.start = start;
        this.stop = stop;
    }

    @Override
    public Optional<Integer> next() {
        Optional<Integer> next = calculateNext(count);
        this.count++;
        return next;
    }

    @Override
    public void reset() {
        this.count = 0;
    }

    @Override
    public boolean isOver() {
        return this.start + count == this.stop;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> listRemain = new ArrayList<>();
        Optional<Integer> next;
        int index = count;
        do {
            next = calculateNext(index);
            index++;
            if(next.isPresent())
                listRemain.add(next.get());
        } while (next.isPresent());
        return listRemain;
    }

    private Optional<Integer> calculateNext(int index) {
        Optional<Integer> returnable = Optional.of(this.start + index);
        if(returnable.get() > this.stop)
            returnable = Optional.empty();

        return returnable;
    }
}
