package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private int sequenceSize;
    private int count = 1;

    public RandomGenerator(int n){
        this.sequenceSize = n;
    }

    @Override
    public Optional<Integer> next() {
        if(this.count >= this.sequenceSize)
            return Optional.empty();
        else {
            Double randomNumber = Math.random();
            this.count++;
            if(randomNumber > 0.5)
                return Optional.of(1);
            else
                return Optional.of(0);
        }
    }

    @Override
    public void reset() {
        this.count = 0;
    }

    @Override
    public boolean isOver() {
        return this.count == this.sequenceSize;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remained = new ArrayList<>();
        for(int i = this.count; i <= this.sequenceSize; i++) {
            Double randomNum = Math.random();
            if (randomNum > 0.5)
                remained.add(1);
            else
                remained.add(0);
        }

        return remained;
    }
}
