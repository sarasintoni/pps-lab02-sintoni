package u02lab.code;

import java.util.Optional;

/**
 * Created by sara.sintoni4 on 07/03/2017.
 */
public class GeneratorFactory {
    public SequenceGenerator getGenerator(Optional<Integer> start, Optional<Integer> stop) {
        if(!start.isPresent())
            return new RandomGenerator(stop.get());
        else
            return new RangeGenerator(start.get(), stop.get());
    }
}
