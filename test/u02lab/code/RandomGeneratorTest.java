package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by sara.sintoni4 on 07/03/2017.
 */
public class RandomGeneratorTest {
    private static final int SEQUENCE_SIZE = 8;

    RandomGenerator randomGenerator;

    @Before
    public void init() {
        randomGenerator = new RandomGenerator(SEQUENCE_SIZE);
    }

    @Test
    public void checkNext() throws Exception {
        for(int i = 1; i < SEQUENCE_SIZE; i++) {
            Optional<Integer> next = randomGenerator.next();
            assertTrue(next.get() == 0 || next.get() == 1);
        }

        Optional<Integer> next = randomGenerator.next();
        assertFalse(next.isPresent());
    }

    @Test
    public void checkEndAndReset() throws Exception {
        for(int i = 1; i < SEQUENCE_SIZE; i++) {
            randomGenerator.next();
        }
        assertTrue(randomGenerator.isOver());
        randomGenerator.reset();
        assertFalse(randomGenerator.isOver());
    }

    @Test
    public void checkListRemaining() throws Exception {
        randomGenerator.next();
        assertTrue(randomGenerator.allRemaining().size() ==
                SEQUENCE_SIZE - 1);
    }
}