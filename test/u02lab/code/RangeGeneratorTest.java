package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by sara.sintoni4 on 07/03/2017.
 */
public class RangeGeneratorTest {
    private static final int START_NUM = 5;
    private static final int END_NUM = 10;

    private RangeGenerator rangeGenerator;

    @Before
    public void init() {
        rangeGenerator = new RangeGenerator(START_NUM, END_NUM);
    }

    @Test
    public void checkNext() throws Exception {
        Optional<Integer> num = rangeGenerator.next();
        assertTrue(num.get().equals(START_NUM+1));
    }

    @Test
    public void checkEmptyAtTheEndOfList() throws Exception {
        for(int j = START_NUM; j<=END_NUM; j++){
            rangeGenerator.next();
        }
        assertFalse(rangeGenerator.next().isPresent());
    }

    @Test
    public void checkReset() throws Exception {
        for(int i = 0; i < 2; i++) {
            rangeGenerator.next();
        }
        rangeGenerator.reset();
        assertTrue(rangeGenerator.next().get().equals(START_NUM));
    }

    @Test
    public void checkIsOver() throws Exception {
        for(int i = START_NUM; i < END_NUM - 1; i++) {
            assertFalse(rangeGenerator.isOver());
            rangeGenerator.next();
        }
        assertTrue(rangeGenerator.isOver());
    }

    @Test
    public void checkListRemaining() throws Exception {
        rangeGenerator.next();
        List<Integer> remain = new ArrayList<>();
        for(int i = START_NUM + 2 ; i <= END_NUM; i++) {
            remain.add(i);
        }
        assertEquals(rangeGenerator.allRemaining(), remain);
        /*assertTrue(rangeGenerator.allRemaining().size() ==
            END_NUM - START_NUM - 1);*/
    }
}