package u02lab.code;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by sara.sintoni4 on 07/03/2017.
 */
public class GeneratorFactoryTest {
    @Test
    public void checkFactory() {
        GeneratorFactory factory = new GeneratorFactory();
        SequenceGenerator range = factory.getGenerator(
                Optional.of(5), Optional.of(10));
        assertTrue(range.next().get() > 1);

        SequenceGenerator random = factory.getGenerator(
                Optional.empty(), Optional.of(8));
        Optional<Integer> nextRandom = random.next();
        assertTrue(nextRandom.get() == 0
                || nextRandom.get() == 1);
    }
}